#ifndef TPMC_UTILITIES_HH
#define TPMC_UTILITIES_HH

#include <dune/common/fvector.hh>
#include <tpmc/fieldtraits.hh>

// specialize tpmc::FieldTraits for Dune::FieldVector
namespace tpmc
{
  template <class T, int dim>
  struct FieldTraits<Dune::FieldVector<T, dim> >
  {
    typedef T field_type;
  };
}

#endif // TPMC_UTILITIES_HH
