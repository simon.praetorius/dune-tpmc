#ifndef DUNE_MARCHINGCUBES_NONDEGENERATINGTHRESHOLDFUNCTOR_HH
#define DUNE_MARCHINGCUBES_NONDEGENERATINGTHRESHOLDFUNCTOR_HH

#include <tpmc/thresholdfunctor.hh>

namespace Dune
{
  namespace MarchingCubes
  {
    template <class vtype, class Geometry>
    class NonDegeneratingThresholdFunctor : public tpmc::ThresholdFunctor<vtype>
    {
    public:
      using tpmc::ThresholdFunctor<vtype>::getDistance;

      //! constructor
      NonDegeneratingThresholdFunctor(const Geometry& _embedding, const vtype _minLength)
          : tpmc::ThresholdFunctor<vtype>(0)
          , embedding(_embedding)
          , minLength(_minLength)
      {
      }

      //! minimum length for an edge below which it will be considered
      //! to be degenerated
      vtype degenerationDistance() const { return 0; }

      //! distance to iso surface
      template <typename point>
      vtype interpolationFactor(const point& a, const point& b, const vtype va,
                                const vtype vb) const
      {
        const vtype length = (embedding.global(a) - embedding.global(b)).two_norm();
        const vtype factor = getDistance(va) / (getDistance(vb) - getDistance(va));
        const vtype sign = factor / std::abs(factor);
        if (length * std::abs(factor) < minLength)
          return sign * minLength / length;
        else if (length - length * std::abs(factor) < minLength)
          return sign * (1.0 - minLength / length);
        else
          return factor;
      }

    private:
      const Geometry embedding;
      const vtype minLength;
    };
  }
}

#endif // DUNE_MARCHINGCUBES_NONDEGENERATINGTHRESHOLDFUNCTOR_HH
